// ====================================================================================================
// Imports
// ====================================================================================================


// Node
var async = require("async");
var util = require("util");

// Third Party
var three = require("three");
var WebSocketServer = require("ws").Server

// App
var constants = require("./common/constants");
var tileFactory = require("./tileFactory.js")();
var projectileFactory = require("./projectileFactory.js")();
var spiderFactory = require("./spiderFactory.js")(
	function(spider){
		tileFactory.highlightMoveableTiles(
			spider.userData.tile.userData.gridCoordinates
		);
	}
);


//====================================================================================================
// WEB SOCKET SERVER
//====================================================================================================


var wss = new WebSocketServer({port: 8080}); 
var clientDict = {};

wss.on("connection", function(ws){

	var sessionId = guid();
	ws.sessionId = sessionId;
	clientDict[sessionId] = ws;

	// Create spider for this client
	var clientSpider = spiderFactory.getFreeSpider();
	placeSpiderAtRandom(clientSpider);
	if(clientSpider){
		clientSpider.userData.activate(sessionId);
	}

	// Wrapper that always stringifies JSON before sending
	ws.sendWrap = function(data){
		ws.send(
			JSON.stringify(data)
		);
	};

	// Send login message along with relevant map and user data
	ws.sendWrap({
		payload: {
			actionId: constants.ACTION_ID_CONNECT,
			sessionId: sessionId,
			tileBuffer: tileFactory.getTilePositionBuffer(),
			spiderBuffer: spiderFactory.getSpiderBuffer()
		}
	});

	// Handle messages recieved from this client
	ws.on("message", function(data, flags){

		try{

			// Parse client requests here
			data = JSON.parse(data);

			// On jump
			if(data.payload && data.payload.actionId == constants.ACTION_ID_JUMP){
				var spiderUuid = data.payload.spiderUuid;
				var tileKey = data.payload.tileKey;
				var spider = spiderFactory.spiderDictionary[spiderUuid];
				var tile = tileFactory.getTileWithKey(tileKey);
				
				var destination = tile.position.clone();

				// Add normal height offset
				destination.setY(destination.y + (constants.BLOCK_SIZE * 0.5));

				// Add extra height for each object stacked on tile
				var stackHeight = Object.keys(tile.userData.stackDict).length * constants.BLOCK_SIZE;
				destination.setY(destination.y + stackHeight);

				spider.userData.destination = destination;
				spider.userData.destinationTile = tile;
			}

			// On fire
			if(data.payload && data.payload.actionId == constants.ACTION_ID_FIRE){
				var spiderUuid = data.payload.spiderUuid;
				var spider = spiderFactory.spiderDictionary[spiderUuid];
				var tileKey = data.payload.tileKey;
				var tile = tileFactory.getTileWithKey(tileKey);

				var projectile = projectileFactory.getFreeProjectile();
				projectile.position.copy(spider.position);
				projectile.userData.activate(
					guid()
				);
				
				var destination = tile.position.clone();

				// Add normal height offset
				destination.setY(destination.y + (constants.BLOCK_SIZE * 0.5));

				// Add extra height for each object stacked on tile
				var stackHeight = Object.keys(tile.userData.stackDict).length * constants.BLOCK_SIZE;
				destination.setY(destination.y + stackHeight);

				projectile.userData.destination = destination;
				projectile.userData.destinationTile = tile;
			}

		}catch(err){
			console.log(util.inspect(err));
		}

	});

	// Handle when a player leaves
	ws.on("close", function(data, flags){
		
		console.log("connection closed");
		
		spiderFactory.spiderDictionary[sessionId].userData.deactivate();
		delete clientDict[sessionId];
		delete ws;
	});

});

wss.broadcast = function broadcast(data){

	wss.clients.forEach(
		function(ws){
			try{
				ws.sendWrap(data);
			}catch(err){
				// TODO: handle code for when client is closed mid-broadcast
				console.log(util.inspect(err));
			}
		}
	);

};


//====================================================================================================
// GAME DATA
//====================================================================================================


// Initialize scene
// Make map
// Buffer N spiders (Buffered but not active/visible until players join)
// Start server
// Start game simulation loop

var scene = null;

async.series(
	[
		function(callback){
			// Initialize scene
			scene = new three.Scene();
			callback(null, "initializeScene");
		},
		function(callback){
			// Create tiles
			tileFactory.createTiles(scene);
			callback(null, "createTiles");
		},
		function(callback){
			// Create spiders
			spiderFactory.createSpiders(scene);
			callback(null, "createSpiders");
		},
		function(callback){
			// Create projectiles
			projectileFactory.createProjectiles(scene);
			callback(null, "createProjectiles");
		}
	],
	function(err, result){

		gameLoop();

	}
);


//====================================================================================================
// GAME LOOP
//====================================================================================================


function gameLoop(){

	// Run this function 30 times a second (1000ms / 30frames)
	setTimeout(
		gameLoop,
		33.3
	);

	// Update object positions
	spiderFactory.updateSpiders();
	projectileFactory.updateProjectiles();

	// Send game state to all clients
	wss.broadcast({
		payload: {
			actionId: constants.ACTION_ID_SNAPSHOT,
			spiderBuffer: spiderFactory.getSpiderBuffer(),
			projectileBuffer: projectileFactory.getProjectileBuffer()
		}
	});

}


//====================================================================================================
// HELPER FUNCTIONS
//====================================================================================================


function guid(){
        function s4(){
                return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
        }
  
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

function placeSpiderAtRandom(spider){

	var values = tileFactory.getRandomTileAndFace();
	var tile = values[0];
	var face = values[1];
	spider.userData.setPositionForTile(tile);

}


//====================================================================================================
// EOF
//====================================================================================================
