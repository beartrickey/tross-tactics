//=============================================================
// Gamepad
//=============================================================

Gamepad = function(){

	var debug = false;
	var axesStates = {};
	var buttonStates = {};
	var buttonPressStates = {};
	var buttonReleaseStates = {};

	return {
		getKey: function(keyCode){return buttonStates[keyCode];},
		getPress: function(keyCode){return buttonPressStates[keyCode];},
		getRelease: function(keyCode){return buttonReleaseStates[keyCode];},
		getAxes: function(index){return axesStates[index];},
		setDebug: function(_debug){debug = _debug;},
		poll: function(){

			var gamepads = navigator.getGamepads();
			var gamepad = gamepads[0];

			if(gamepad == null){return;}

			for(b in gamepad.buttons){
				// Trigger press if going from up to down
				if(!buttonStates[b] && gamepad.buttons[b].pressed){
					buttonPressStates[b] = true;
					buttonReleaseStates[b] = false;
				}

				// Trigger release if going from down to up
				if(buttonStates[b] && !gamepad.buttons[b].pressed){
					buttonPressStates[b] = false;
					buttonReleaseStates[b] = true;
				}

				buttonStates[b] = gamepad.buttons[b].pressed;
			}
			for(a in gamepad.axes){
				axesStates[a] = gamepad.axes[a];
			}

		},
		flushPressReleaseState: function(){
			buttonPressStates = {};
			buttonReleaseStates = {};
		}
	};
};

module.exports = Gamepad;
