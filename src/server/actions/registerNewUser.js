var async = require("async");
var constants = require("../../common/constants.js");

function registerNewUser(wss, ws, data, redis){

	var requestedPlayerName = data.payload.playerName;

	async.series([

		// CHECK IF NAME ALREADY EXISTS
		function(callback){

			redis.sismember(
				"usernames",
				requestedPlayerName,
				function(err, result){
					if(result){
						// Name already exists
						callback("error");
					}
					else{
						callback();
					}
				}
			)
		},

		// STORE NEW USER
		function(callback){

			// Store name
			ws.playerName = data.payload.playerName;

			redis.sadd(
				"usernames",
				ws.playerName
			);
			
			callback();
		}],

		// ON SERIES END
		function(err){

			if(err){
				// SEND FAIL MESSAGE
				ws.sendWrap({
					"status": constants.STATUS_CODE_FAIL,
					"message": "Username " + requestedPlayerName + " already taken."
				});
			}else{
				// SEND SUCCESS MESSAGE
				ws.sendWrap({
					"status": constants.STATUS_CODE_SUCCESS
				});
			}

		}
	);
};

module.exports = registerNewUser;

