var three = require("three");
var constants = require("./common/constants.js");

SpiderFactory = function(
	setPositionForTileCallback
){

	var spiders = [];
	var spiderDictionary = {};
	var width = 50.0;
	var height = 50.0;

	var spiderMaterial = new three.MeshBasicMaterial({
		vertexColors: three.FaceColors
	});

	var spiderGeometry = new three.BoxGeometry(width, height, width);

	var createName = function(){

		var firstNames = [
			"Poindexter",
			"Harpalus",
			"Cato",
			"Eureka",
			"Yert"
		];

		var lastNames = [
			"The Third",
			"Jones",
			"Kanemoto",
			"Yamaha",
			"Nakata"
		];

		var randFirstIndex = Math.floor(Math.random() * firstNames.length);
		var randLastIndex = Math.floor(Math.random() * lastNames.length);
		var firstName = firstNames[randFirstIndex];
		var lastName = lastNames[randLastIndex];

		return firstName + " " + lastName;
	};

	var createSpider = function(scene){

		spiderMaterial.skinning = true;
		spiderMaterial.side = three.DoubleSide;
		var mesh = new three.SkinnedMesh(
			spiderGeometry,
// 			new three.MeshFaceMaterial([spiderMaterial])
			spiderMaterial
		);

		// Animation
		if(spiderGeometry.animations){
			mesh.userData.animationMixer = new three.AnimationMixer(mesh);

			for(var i = 0; i < spiderGeometry.animations.length; ++ i){
				mesh.userData.animationMixer.clipAction(spiderGeometry.animations[i]);
			}

			mesh.userData.animationMixer.clipAction(mesh.geometry.animations[1])
				.setDuration(1)	// 1 second
				.startAt(0)
				.play();
		}

		mesh.translateX(0.0);
		mesh.translateY(300.0);
		mesh.translateZ(0.0);
		mesh.visibility = false;
		

		mesh.userData.gameObjectType = constants.TYPE_SPIDER;
		mesh.userData.collision = true;

		mesh.userData.name = createName();
		mesh.userData.face = null;
		mesh.userData.tile = null;
		mesh.userData.destination = null;
		mesh.userData.destinationTile = null;
		mesh.userData.velocity = null;
		mesh.userData.active = false;
		mesh.userData.jumping = false;

		mesh.userData.activate = function(uuid){

			mesh.userData.uuid = uuid;
			mesh.userData.active = true;
			mesh.visible = true;
			scene.add(mesh);
			spiderDictionary[uuid] = mesh;

		};

		mesh.userData.deactivate = function(){

			console.log("deactivate");
			var uuid = mesh.userData.uuid;

			if(mesh.userData.tile){
				delete mesh.userData.tile.userData.stackDict[mesh.userData.uuid];
			}

			mesh.userData.active = false;
			mesh.userData.jumping = false;
			mesh.userData.uuid = null;
			mesh.userData.velocity = null;
			mesh.userData.destinationTile = null;
			mesh.userData.destination = null;
			mesh.visible = false;
			scene.remove(mesh);
			delete spiderDictionary[uuid];

		};

		mesh.userData.move = function(){

			// Set velocity
			if(mesh.userData.destination && mesh.userData.velocity === null){

				// Set jumping flag
				mesh.userData.jumping = true;

				// Remove this from starting tile's objectStack
				delete mesh.userData.tile.userData.stackDict[mesh.userData.uuid];

				var direction = mesh.position.clone()
					.sub(mesh.userData.destination)
					.multiply(new three.Vector3(-0.5, -0.5, -0.5));

				// Increase height depending on distance
				var distanceToDestination = mesh.position.distanceTo(mesh.userData.destination);
				direction.setY(750.0 * 0.75);

				// Set final velocity
				var distanceToMidpoint = mesh.position.distanceTo(direction.clone().add(mesh.position));
				var speed = 50.0;

				// Don't use as much speed if dropping to lower position
				if(mesh.position.y > mesh.userData.destination.y){
					speed *= 0.9;
				}

				mesh.userData.velocity = direction.normalize().multiplyScalar(speed);

				// Update facing
				mesh.userData.direction = mesh.userData.velocity.clone()
					.multiply(new three.Vector3(1.0, 0.0, 1.0))
					.normalize();

				mesh.up = new three.Vector3(0.0, 1.0, 0.0);
				mesh.userData.lookAtPos = mesh.userData.direction.clone()
					.multiplyScalar(-100.0)
					.add(mesh.position);

				mesh.lookAt(mesh.userData.lookAtPos);

			}

			if(mesh.userData.velocity){

				var newPosition = mesh.position.clone().add(
					mesh.userData.velocity
				);

				mesh.position.set(
					newPosition.x,
					newPosition.y,
					newPosition.z
				);

				// Apply gravity
				var gravity = 3.6 * 1.25;
				mesh.userData.velocity.setY(mesh.userData.velocity.y - gravity);


				// Stop when close enough to destination
				var distance = mesh.position.distanceTo(mesh.userData.destination);
				if(
					(
						distance <= 50.0
						&& mesh.userData.velocity.y < 0.0
					) ||
					(
						mesh.position.y < (mesh.userData.destinationTile.position.y + (constants.BLOCK_SIZE * 0.5))
						&& mesh.userData.velocity.y < 0.0
					)
				){
					mesh.userData.setPositionForTile(
						mesh.userData.destinationTile
					);
					mesh.userData.destinationTile.userData.stackDict[mesh.userData.uuid] = mesh;
					mesh.userData.velocity = null;
					mesh.userData.destinationTile = null;
					mesh.userData.destination = null;
					mesh.userData.jumping = false;
				}

			}

		};

		mesh.userData.setPositionForTile = function(newTile){

			// HACK: only use upward normal
			var faceNormal = new three.Vector3(0.0, 1.0, 0.0);

			// Make spider face direction of movement
			var distance = constants.BLOCK_SIZE * 0.5; // Distance from block
			var newPosition = newTile.position.clone().add(
				faceNormal.clone().multiplyScalar(distance)
			);

			// Update the geometry
			mesh.position.set(
				newPosition.x,
				newPosition.y,
				newPosition.z
			);
			var stackHeight = Object.keys(newTile.userData.stackDict).length;
			mesh.position.setY(
				mesh.position.y + (stackHeight * constants.BLOCK_SIZE)
			);


			mesh.userData.tile = newTile;

			// Trigger callback
			setPositionForTileCallback(mesh);
		}

		return mesh;

	};

	return {
		setSpiderMaterial: function(spiderTexture){

			spiderMaterial = new three.MeshBasicMaterial({
				map: spiderTexture,
				vertexColors: three.FaceColors,
				transparent: true,
				side: three.DoubleSide
			});

		},

		setSpiderGeometry: function(_spiderGeometry){

			spiderGeometry = _spiderGeometry;

		},

		createSpiders: function(scene){
			for(var s = 0; s < constants.NUM_SPIDERS; s++){
				var spider = createSpider(scene);
				spider.scale.set(45.0, 45.0, 45.0);
				spiders.push(spider);
			}
		},

		getSpider: function(index){
			return spiders[index];
		},

		getFreeSpider: function(){
			for(var s in spiders){
				var spider = spiders[s];
				if(!spider.userData.active){
					return spider;
				}
			}
		},

		playMeshAnimations: function(dt){
			for(var s in spiders){
				var spider = spiders[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				// Update spider positions
				spider.userData.animationMixer.update(dt);
			}
		},

		updateSpiders: function(){
			for(var s in spiders){
				var spider = spiders[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				// Update spider positions
				spider.userData.move();
			}
		},

		getSpiderAtTile: function(tile){
			for(var s in spiders){
				var spider = spiders[s];
				if(spider.userData.tile === tile){
					return spiders[s];
				}
			}
			return null;
		},

		getSpiderBuffer: function(){

			var returnDictionary = {};
	
			for(var s in spiderDictionary){
				var spider = spiderDictionary[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				returnDictionary[s] = [
					spider.position.x,
					spider.position.y,
					spider.position.z,
					spider.rotation.x,
					spider.rotation.y,
					spider.rotation.z,
					spider.userData.tile.userData.tileKey,
					spider.userData.jumping
				];
			}

			return returnDictionary;
		},

		spiders: spiders,
		spiderDictionary: spiderDictionary
	};
};

module.exports = SpiderFactory;

