var three = require("three");
var constants = require("./common/constants.js");
var SnapToGridComponent = require("./components/snapToGrid.js");

Item = function(){

	var itemMaterial = new three.MeshBasicMaterial({
		vertexColors: three.FaceColors
	});

	var geometry = new three.BoxGeometry(width, height, width);

	// Left
	geometry.faces[0].color = new three.Color(0xffff00);
	geometry.faces[1].color = new three.Color(0xffff00);
	// Right
	geometry.faces[2].color = new three.Color(0xffff00);
	geometry.faces[3].color = new three.Color(0xffff00);
	// Top
	geometry.faces[4].color = new three.Color(0xffff00);
	geometry.faces[5].color = new three.Color(0xffff00);
	// Botom
	geometry.faces[6].color = new three.Color(0xffff00);
	geometry.faces[7].color = new three.Color(0xffff00);
	// Front
	geometry.faces[8].color = new three.Color(0xffff00);
	geometry.faces[9].color = new three.Color(0xffff00);
	// Back
	geometry.faces[10].color = new three.Color(0xffff00);
	geometry.faces[11].color = new three.Color(0xffff00);

	var mesh = new three.Mesh(geometry, itemMaterial);
	mesh.translateX(0.0);
	mesh.translateY(300.0);
	mesh.translateZ(0.0);

	mesh.userData.gameObjectType = constants.TYPE_ITEM;
	mesh.userData.componentDict = {};
	mesh.userData.componentDict["snapToGrid"] = SnapToGridComponent();
	mesh.userData.collision = true;
	mesh.userData.face = face;
	mesh.userData.tile = tile;

	return mesh;

};

module.exports = Item;

