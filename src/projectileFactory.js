var three = require("three");
var constants = require("./common/constants.js");

ProjectileFactory = function(){

	var projectiles = [];
	var projectileDictionary = {};
	var width = 50.0;
	var height = 50.0;

	var projectileMaterial = new three.MeshBasicMaterial({
		vertexColors: three.FaceColors
	});

	var projectileGeometry = new three.BoxGeometry(width, height, width);

	var createProjectile = function(scene){

		projectileMaterial.skinning = true;
		projectileMaterial.side = three.DoubleSide;
		var mesh = new three.SkinnedMesh(
			projectileGeometry,
// 			new three.MeshFaceMaterial([projectileMaterial])
			projectileMaterial
		);

		// Animation
		if(projectileGeometry.animations){
			mesh.userData.animationMixer = new three.AnimationMixer(mesh);

			for(var i = 0; i < projectileGeometry.animations.length; ++ i){
				mesh.userData.animationMixer.clipAction(projectileGeometry.animations[i]);
			}

			mesh.userData.animationMixer.clipAction(mesh.geometry.animations[1])
				.setDuration(1)	// 1 second
				.startAt(0)
				.play();
		}

		mesh.visibility = false;
		
		mesh.userData.gameObjectType = constants.TYPE_PROJECTILE;

		mesh.userData.destination = null;
		mesh.userData.destinationTile = null;
		mesh.userData.velocity = null;
		mesh.userData.active = false;

		mesh.userData.activate = function(uuid){

			mesh.userData.uuid = uuid;
			mesh.userData.active = true;
			mesh.visible = true;
			scene.add(mesh);
			projectileDictionary[uuid] = mesh;

		};

		mesh.userData.deactivate = function(){

			console.log("deactivate");
			var uuid = mesh.userData.uuid;

			mesh.userData.active = false;
			mesh.userData.uuid = null;
			mesh.userData.velocity = null;
			mesh.userData.destinationTile = null;
			mesh.userData.destination = null;
			mesh.visible = false;
			scene.remove(mesh);
			delete projectileDictionary[uuid];

		};

		mesh.userData.move = function(){

			// Set velocity
			if(mesh.userData.destination && mesh.userData.velocity === null){

				var direction = mesh.position.clone()
					.sub(mesh.userData.destination)
					.multiply(new three.Vector3(-0.5, -0.5, -0.5));

				// Increase height depending on distance
				var distanceToDestination = mesh.position.distanceTo(mesh.userData.destination);
				direction.setY(750.0 * 0.75);

				// Set final velocity
				var distanceToMidpoint = mesh.position.distanceTo(direction.clone().add(mesh.position));
				var speed = 50.0;

				// Don't use as much speed if dropping to lower position
				if(mesh.position.y > mesh.userData.destination.y){
					speed *= 0.9;
				}

				mesh.userData.velocity = direction.normalize().multiplyScalar(speed);

				// Update facing
				mesh.userData.direction = mesh.userData.velocity.clone()
					.multiply(new three.Vector3(1.0, 0.0, 1.0))
					.normalize();

				mesh.up = new three.Vector3(0.0, 1.0, 0.0);
				mesh.userData.lookAtPos = mesh.userData.direction.clone()
					.multiplyScalar(-100.0)
					.add(mesh.position);

				mesh.lookAt(mesh.userData.lookAtPos);

			}

			if(mesh.userData.velocity){

				var newPosition = mesh.position.clone().add(
					mesh.userData.velocity
				);

				mesh.position.set(
					newPosition.x,
					newPosition.y,
					newPosition.z
				);

				// Apply gravity
				var gravity = 3.6 * 1.25;
				mesh.userData.velocity.setY(mesh.userData.velocity.y - gravity);


				// Stop when close enough to destination
				var distance = mesh.position.distanceTo(mesh.userData.destination);
				if(
					mesh.position.y < (mesh.userData.destinationTile.position.y + (constants.BLOCK_SIZE * 0.5))
					&& mesh.userData.velocity.y < 0.0
				){
					// Make explosion
					// Deactivate
					mesh.userData.deactivate();
				}

			}

		};

		return mesh;

	};

	return {
		setProjectileMaterial: function(projectileTexture){

			projectileMaterial = new three.MeshBasicMaterial({
				map: projectileTexture,
				vertexColors: three.FaceColors,
				transparent: true,
				side: three.DoubleSide
			});

		},

		setProjectileGeometry: function(_projectileGeometry){

			projectileGeometry = _projectileGeometry;

		},

		createProjectiles: function(scene){
			for(var s = 0; s < constants.NUM_SPIDERS; s++){
				var projectile = createProjectile(scene);
				projectile.scale.set(20.0, 20.0, 20.0);
				projectiles.push(projectile);
			}
		},

		getProjectile: function(index){
			return projectiles[index];
		},

		getFreeProjectile: function(){
			for(var s in projectiles){
				var projectile = projectiles[s];
				if(!projectile.userData.active){
					return projectile;
				}
			}
		},

		playMeshAnimations: function(dt){
			for(var s in projectiles){
				var projectile = projectiles[s];

				// Skip inactive projectiles
				if(!projectile.userData.active){continue;}

				// Update projectile positions
				projectile.userData.animationMixer.update(dt);
			}
		},

		updateProjectiles: function(){
			for(var s in projectiles){
				var projectile = projectiles[s];

				// Skip inactive projectiles
				if(!projectile.userData.active){continue;}

				// Update projectile positions
				projectile.userData.move();
			}
		},

		getProjectileBuffer: function(){

			var returnDictionary = {};
	
			for(var s in projectileDictionary){
				var projectile = projectileDictionary[s];

				// Skip inactive projectiles
				if(!projectile.userData.active){continue;}

				returnDictionary[s] = [
					projectile.position.x,
					projectile.position.y,
					projectile.position.z,
					projectile.rotation.x,
					projectile.rotation.y,
					projectile.rotation.z,
				];
			}

			return returnDictionary;
		},

		projectiles: projectiles,
		projectileDictionary: projectileDictionary
	};
};

module.exports = ProjectileFactory;

