// Node
var async = require("async");
var util = require("util");

// Third Party
var WebSocket = require("ws");
var ws = new WebSocket("ws://localhost:8080");

// Added functionality
ws.connected = false;

ws.sendWrap = function(data){
	ws.send(
		JSON.stringify(data)
	);
};

// On connection open
ws.on("open", function(){
	ws.send(JSON.stringify({something: "something_else"}));
	ws.connected = true;
});

ws.on("message", function(data, flags) {
	// flags.binary will be set if a binary data is received.
	// flags.masked will be set if the data was masked.
	console.log(data);


	// Received map data
	// position tile geometry

	// Received world state
	// position spider geometry, etc


});


// Prompt mechanism
process.stdin.setEncoding("utf8");
process.stdin.on("data", function(val){
	ws.send(val);
});


