//====================================================================================================
// IMPORTS
//====================================================================================================


// Node
var util = require("util");
var url = require("url");

// Third-party
var async = require("async");
var three = require("three");

// App
var constants = require("./common/constants.js");

var Keyboard = require("./keyboard.js");
var keyboard = new Keyboard();

var Gamepad = require("./gamepad.js");
var gamepad = new Gamepad();

var TileFactory = require("./tileFactory.js");
var tileFactory = TileFactory();

var SpiderFactory = require("./spiderFactory.js");
var spiderFactory = SpiderFactory(
	function(spider){
		tileFactory.highlightMoveableTiles(
			spider.userData.tile.userData.gridCoordinates,
			0x0000ff
		);
	}
);

var ProjectileFactory = require("./projectileFactory.js");
var projectileFactory = ProjectileFactory();



//====================================================================================================
// DEVICE DETECTION
//====================================================================================================


var device = constants.DEVICE_TYPE_DESKTOP;
function detectDevice(){
	if(navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i)){
		device = constants.DEVICE_TYPE_MOBILE;
	}else if(navigator.userAgent.match(/iPad/i)){
		device = constants.DEVICE_TYPE_TABLET;
	}
}


//====================================================================================================
// DOM ELEMENTS
//====================================================================================================


var domMainContainer = null;
var domMainContainerRect = null;
var screenWidth = 0.0;
var screenHeight = 0.0;

function setupDom(){

	// Figure out screen resolution
	screenWidth = document.body.clientWidth;;
	screenHeight = document.body.clientHeight;;

	if(screenWidth > constants.SCREEN_WIDTH_MAX){screenWidth = constants.SCREEN_WIDTH_MAX;}
	if(screenHeight > constants.SCREEN_HEIGHT_MAX){screenHeight = constants.SCREEN_HEIGHT_MAX;}

	console.log("document.body size");
	console.log([document.body.clientWidth, document.body.clientHeight]);
	console.log("clamped screen size");
	console.log([screenWidth, screenHeight]);

	// Set main-container size
	domMainContainer = document.getElementById("main-container");
	domMainContainer.style.width = screenWidth + "px";
	domMainContainer.style.height = screenHeight + "px";
	domMainContainer.style.minWidth =  screenWidth + "px";
	domMainContainer.style.minHeight = screenHeight + "px";
	domMainContainer.style.maxWidth = screenWidth + "px";
	domMainContainer.style.maxHeight = screenHeight + "px";

	arrangeDomElements();

}


function arrangeDomElements(){

	domMainContainerRect = domMainContainer.getBoundingClientRect();

	$("#crosshairs").css("position", "absolute").
	css("left", (domMainContainerRect.left + (screenWidth * 0.5) - 32) + "px").
	css("top", (domMainContainerRect.bottom - 96) + "px");

}


//====================================================================================================
// GAME VARIABLES
//====================================================================================================


var selectedSpider = null;
var selectedTile = null;
var selectedFace = null;
var fireMode = false;
var fireModeRaycaster = null;


//====================================================================================================
// MOUSE CONTROLS
//====================================================================================================


var lastMousePosition = new three.Vector2(0.0, 0.0);
var mouseIsDown = false;
var mouseMovedAfterDown = false;
var mouseDownCounter = 0;
var uiElementPrecedent = false;


function onRightMouseClick(e){

	e.preventDefault();

};


function onMouseMove(e){

	e.preventDefault();

	mouseMovedAfterDown = true;

	// Skip if mousemove event but mouse isn't down
	if(e.type === "mousemove" && mouseIsDown === false)
		return;

	var currentMousePosition = getMouseCoords(e);
	var xDif = lastMousePosition.x - currentMousePosition[0];
	var yDif = lastMousePosition.y - currentMousePosition[1];

	// Rotate camera swivel
	cameraSwivelObjectX.rotation.x += yDif * constants.MOUSE_ROTATE_SENSITIVITY;
	cameraSwivelObjectY.rotation.y += xDif * constants.MOUSE_ROTATE_SENSITIVITY;

	lastMousePosition = new three.Vector2(
		currentMousePosition[0],
		currentMousePosition[1]
	)

}


function onMouseDown(e){

	mouseIsDown = true;
	mouseMovedAfterDown = false;

	var currentMousePosition = getMouseCoords(e);
	lastMousePosition = new three.Vector2(
		currentMousePosition[0],
		currentMousePosition[1]
	)

	// Return camera to origin with 0
	if(keyboard.getKey("48")){
		cameraSwivelObjectBase.position.set(0, 0, 0);
		cameraSwivelObjectBase.rotation.set(0, 0, 0);
		cameraSwivelObjectX.rotation.x = Math.PI * -0.15;
		cameraSwivelObjectY.rotation.y = Math.PI * 0.25
	}

	// Create tile with c
	if(keyboard.getKey("67")){
		
		var x = selectedTile.userData.gridCoordinates.x + selectedFace.normal.x;
		var y = selectedTile.userData.gridCoordinates.y + selectedFace.normal.y;
		var z = selectedTile.userData.gridCoordinates.z + selectedFace.normal.z;
		tileFactory.createTileAtPosition(x, y, z, scene);
		tileFactory.shadeTiles();
	}

}


function onUiElementUp(e){

	console.log("onUiElementUp");
	uiElementPrecedent = true;

}


function onMouseUp(e){

	console.log("onMouseUp");

	// Only handle collision with 3D elements:
	// - If no ui elements were touched first
	// - If the player wasn't dragging/rotating the camera around
	if(uiElementPrecedent === false && mouseMovedAfterDown === false){

		handleTouchCollision();

	}

	mouseDownCounter = 0;
	mouseIsDown = false;
	uiElementPrecedent = false;
}


function onCrosshairsUp(e){

	console.log("onCrosshairsUp");
	// Toggle mode
	if(fireMode === true){
		toggleFireMode(false);
	}else{
		toggleFireMode(true);
	}

}


function toggleFireMode(state){

	fireMode = state;

	if(fireMode === true){

		$("#crosshairs").css("width", "76px");
		$("#crosshairs").css("left", (domMainContainerRect.left + (screenWidth * 0.5) - (76 / 2)) + "px");
		tileFactory.highlightTargetableTiles(
			selectedSpider.userData.tile.userData.gridCoordinates,
			0xff0000
		);
		
	}else{

		$("#crosshairs").css("width", "64px");
		$("#crosshairs").css("left", (domMainContainerRect.left + (screenWidth * 0.5) - 32) + "px");
		tileFactory.highlightMoveableTiles(
			selectedSpider.userData.tile.userData.gridCoordinates,
			0x0000ff
		);

	}

}


function onMouseWheel(e){

	// Commenting out zooming
// 	var wheelDelta = e.wheelDelta;
// 	wheelDelta *= constants.MOUSE_WHEEL_SENSITIVITY;
// 	camera.zoom += wheelDelta;
// 	camera.position.z += 10.0 * wheelDelta;
// 	camera.updateProjectionMatrix();

}


function getMouseCoords(e){

	var posx = 0;
	var posy = 0;

	if(e.pageX && e.pageY){
		posx = e.pageX;
		posy = e.pageY;
	}
	else if(e.originalEvent.targetTouches[0].pageX && e.originalEvent.targetTouches[0].pageY){
		posx = e.originalEvent.targetTouches[0].pageX;
		posy = e.originalEvent.targetTouches[0].pageY;
	}
	else if(e.clientX && e.clientY)
	{
		posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	else if(e.originalEvent.targetTouches[0].clientX && e.originalEvent.targetTouches[0].clientY)
	{
		posx = e.originalEvent.targetTouches[0].clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		posy = e.originalEvent.targetTouches[0].clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}

	return [posx, posy];
}


// ============================================
// GAMEPAD
// ============================================


function checkGamepad(){

	gamepad.poll();

	// Button 0
	if(gamepad.getPress(0)){

		// Toggle mode
		if(fireMode === true){
			toggleFireMode(false);
		}else{
			toggleFireMode(true);
		}

	}

	// Rotate camera swivel with analog sticks
	if(gamepad.getAxes(0)){
		cameraSwivelObjectX.rotation.x += gamepad.getAxes(1) * constants.MOUSE_ROTATE_SENSITIVITY;
		cameraSwivelObjectY.rotation.y += gamepad.getAxes(0) * constants.MOUSE_ROTATE_SENSITIVITY;
	}

	// Reset states every frame
	gamepad.flushPressReleaseState();

}


// ============================================
// KEYBOARD
// ============================================


function checkKeyboard(){

	// Toggle fire mode with f
	if(keyboard.getPress("70")){

		// Toggle mode
		if(fireMode === true){
			toggleFireMode(false);
		}else{
			toggleFireMode(true);
		}

	}

	// Reset states every frame
	keyboard.flushPressReleaseState();

}


//====================================================================================================
// SELECTION
//====================================================================================================


function selectSpider(spider){

	// Players can only select their own spider
	if(spider.userData.uuid != connection.sessionId){return;}

	selectedSpider = spider;

	$("#crosshairs").css("display", "block");

	// Color nearby faces that spider can jump to
	tileFactory.highlightMoveableTiles(
		selectedSpider.userData.tile.userData.gridCoordinates,
		0x0000ff
	);

}


function deselectSpider(){

	selectedSpider = null;
	toggleFireMode(false);

	$("#crosshairs").css("display", "none");

}


// ====================================================================================================
// Textures
// ====================================================================================================


var beigeNoiseTexture = null;
var keeperTexture = null;
function loadTextures(allTexturesLoadedCallback){

 	var tl = new three.TextureLoader();
	async.series(
		[
			function(callback){
				tl.load(
					"/textures/beige_noise.png",
					function(texture){
						texture.wrapS = three.RepeatWrapping;
						texture.wrapT = three.RepeatWrapping;
						beigeNoiseTexture = texture;

						callback(null, "beige texture loaded");
					},
					// Function called when download progresses
					function(xhr){
					},
					// Function called when download errors
					function(xhr){
						console.log( 'An error happened' );
					}
				);
			},
			function(callback){
				tl.load(
					"/textures/keeper_tex.png",
					function(texture){
						texture.wrapS = three.RepeatWrapping;
						texture.wrapT = three.RepeatWrapping;
						keeperTexture = texture;

						callback(null, "keeper texture loaded");
					},
					// Function called when download progresses
					function(xhr){
					},
					// Function called when download errors
					function(xhr){
						console.log( 'An error happened' );
					}
				);
			},
 
		],
		function(err, result){

			allTexturesLoadedCallback(null, "textures loaded");

		}
	);

}


// ====================================================================================================
// Models
// ====================================================================================================


// Three json blender exporter: https://github.com/mrdoob/three.js/tree/master/utils/exporters/blender
// Exporter notes:
// Make sure model is at rest pose when exporting
// Use scale == 1.0 in exporter settings (instead scale in js code)

var keeperGeometry = null;
function loadModels(asyncCallback){

	var cl = new three.JSONLoader();
	cl.load(
 		"/models/keeper.js",
		function(geometry, materials){
			keeperGeometry = geometry;
			asyncCallback(null, "model loaded");
		},
		// Function called when download progresses
		function(xhr){
			console.log("on progress");
		},
		// Function called when download errors
		function(xhr){
			console.log("on error");
		}
	);

}


// ====================================================================================================
// Event Listeners
// ====================================================================================================


function addEventListeners(){

	// EventListeners
	document.body.addEventListener("mousewheel", onMouseWheel, false);
	window.addEventListener("resize", arrangeDomElements);

	if(device === constants.DEVICE_TYPE_DESKTOP){

		$("body").on("mousemove", onMouseMove);
		$("body").on("mousedown", onMouseDown);
		$("body").on("mouseup", onMouseUp);
		$("body").on("contextmenu", onRightMouseClick);

		$(".ui-element").on("mouseup", onUiElementUp);

		$("#crosshairs").get(0).addEventListener("mouseup", onCrosshairsUp);

	}else if(device === constants.DEVICE_TYPE_MOBILE || device === constants.DEVICE_TYPE_TABLET){

		$("body").on("touchstart", onMouseDown);
		$("body").on("touchmove", onMouseMove);
		$("body").on("touchend", onMouseUp);

		$(".ui-element").on("touchend", onUiElementUp);

		$("#crosshairs").get(0).addEventListener("touchend", onCrosshairsUp);
	}

	// HACK: Gamepad API test
// 	window.addEventListener("gamepadconnected", function(e) {
// 		console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
// 		e.gamepad.index, e.gamepad.id,
// 		e.gamepad.buttons.length, e.gamepad.axes.length);
// 	});

}


// ====================================================================================================
// WebSocket Listeners
// ====================================================================================================


// TODO: Move vars to better place
var snapshotBuffer = [];
var clientTime = 0.0;
var connection = null;
function initializeWebSocketConnection(callback){

	// TODO: Add handling for connection timeout

	// Get server from query params if available
	var serverAddress = "localhost:8080";

	var urlComponents = url.parse(window.location.href, true);
	var query = urlComponents.query;
	if(query["serverAddress"]){
		serverAddress = query["serverAddress"];
	}

	// Contact server and retrieve map data
	connection = new WebSocket("ws://" + serverAddress);

	connection.onopen = function(){
		connection.send(JSON.stringify({
			"ping": "ping"
		}));
	};

	connection.onerror = function(error){
		console.log("WebSocket Error: " + error);
	};

	// Handle messages from the server
	connection.onmessage = function(message){
		try{
			// Parse client requests here
			data = JSON.parse(message.data);

			// On connect
			if(data.payload && data.payload.actionId == constants.ACTION_ID_CONNECT){

				console.log("connection successful");

				connection.sessionId = data.payload.sessionId;

				// Create map from server data
				tileFactory.createTilesFromBuffer(
					data.payload.tileBuffer,
					scene
				);
				tileFactory.shadeTiles();

				// Don't callback until connection has been made and data retrieved
				callback(null, "Initialize WebSocket Connection");
			}

			// On snapshot
			if(data.payload && data.payload.actionId == constants.ACTION_ID_SNAPSHOT){

				// Add snapshot to end of buffer
				data.payload.clientTime = getTime();
				snapshotBuffer.push(data.payload);
			}

		}catch(err){
			console.log(util.inspect(err));
			callback(err, "Initialize WebSocket Connection");
		}

	};

}


// ============================================
// Three initialization
// ============================================


var camera = null;
var cameraSwivelObjectBase = new three.Object3D();
var cameraSwivelObjectY = new three.Object3D();
var cameraSwivelObjectX = new three.Object3D();
var scene = null;
var clock = new three.Clock();
var renderer = null;
var raycaster = new three.Raycaster();
var navCubeMesh = null;


$(document).ready(function(){

	// Controls
	keyboard.setDebug(true);

	async.series(
		[
			function(callback){
				// Detect device
				detectDevice();
				callback(null, "Detected device");
			},
			function(callback){
				// DOM initialization
				setupDom();
				callback(null, "Setup DOM");
			},
			function(callback){
				// Add event listeners
				addEventListeners();
				callback(null, "Add event listeners");
			},
			function(callback){
				// Initialize Scene
				initializeScene();
				callback(null, "Initialize scene");
			},
			function(callback){
				// Load textures
				loadTextures(callback);
			},
			function(callback){
				// Setup texture-dependent materials
				tileFactory.setTileMaterial(beigeNoiseTexture);
				spiderFactory.setSpiderMaterial(keeperTexture);
				projectileFactory.setProjectileMaterial(keeperTexture);
				callback(null, "Initialize materials");
			},
			function(callback){
				// Load models
				loadModels(callback);
			},
			function(callback){
				// Setup geometry
				spiderFactory.setSpiderGeometry(keeperGeometry);
				projectileFactory.setProjectileGeometry(keeperGeometry);
				callback(null, "Initialize geometry");
			},
			function(callback){
				spiderFactory.createSpiders(scene);
				projectileFactory.createProjectiles(scene);
				callback(null, "Create spiders");
			},
			function(callback){
				// Initialize WebSocket connection
				console.log(connection);
				initializeWebSocketConnection(callback);
			}
 
		],
		function(err, result){

			console.log(err);
			console.log(result);
			console.log(connection);
			animate();

		}
	);


});


function initializeScene(){

	camera = new three.OrthographicCamera(
		-(screenWidth / 2),
		(screenWidth / 2),
		(screenHeight / 2),
		-(screenHeight / 2),
		8000.0,
		12000.0
	);

	camera.position.z = 10000.0;
	camera.zoom = constants.NORMAL_ZOOM;;
	camera.updateProjectionMatrix();

	// scene > cameraSwivelObjectBase > cameraSwivelObjectY > cameraSwivelObjectX > camera
	cameraSwivelObjectX.add(camera);
	cameraSwivelObjectY.add(cameraSwivelObjectX);
	cameraSwivelObjectBase.add(cameraSwivelObjectY);
	cameraSwivelObjectX.rotation.x = Math.PI * -0.15;
	cameraSwivelObjectY.rotation.y = Math.PI * 0.25
	cameraSwivelObjectBase.position.y = 100.0;
	initCameraPosition();

	scene = new three.Scene();
	scene.add(cameraSwivelObjectBase);
	scene.fog = new three.FogExp2(constants.SKY_COLOR, constants.FOG_DENSITY);
// 	document.body.style["background-color"] = "#" + constants.SKY_COLOR.toString(16);

	renderer = new three.WebGLRenderer();
	renderer.setClearColor(constants.SKY_COLOR);
	renderer.setSize(window.innerWidth, window.innerHeight);


	// Setup navCube
	var navCubeMaterial = new three.MeshBasicMaterial({
		vertexColors: three.FaceColors
	});
	var navCubeGeometry = new three.BoxGeometry(10.0, 10.0, 20.0);
	navCubeMesh = new three.Mesh(navCubeGeometry, navCubeMaterial);
	navCubeMesh.userData.gameObjectType = constants.TYPE_NAVCUBE;
	navCubeMesh.userData.collision = false;
	for(var f in navCubeMesh.geometry.faces){
		navCubeMesh.geometry.faces[f].color = new three.Color(1.0, 0.0, 0.0);
		navCubeMesh.geometry.colorsNeedUpdate = true;
	}

	// Taper shape
	for(var v in navCubeMesh.geometry.vertices){
		var vertex = navCubeMesh.geometry.vertices[v];
		if(vertex.z > 0.0){
			vertex.x = 0.0;
			vertex.y = 0.0;
		}
	}
	scene.add(navCubeMesh);

	domMainContainer.appendChild(renderer.domElement);


	// Set renderer size (appears as "canvas" element in dom)
	$("canvas").css("width", screenWidth + "px");
	$("canvas").css("height", screenHeight + "px");
	$("canvas").css("min-width", screenWidth + "px");
	$("canvas").css("min-height", screenHeight + "px");
	$("canvas").css("max-width", screenWidth + "px");
	$("canvas").css("max-height", screenHeight + "px");

}


function initCameraPosition(){

	cameraSwivelObjectBase.position.set(0, 200, 0);
	camera.position.z = 10000.0;
	camera.zoom = constants.NORMAL_ZOOM;
	camera.near = 8000.0;
	camera.far = 12000;
	camera.updateProjectionMatrix();

}


function handleTouchCollision(){

	var offsetMouseX = lastMousePosition.x - domMainContainerRect.left;
	var offsetMouseY = lastMousePosition.y - domMainContainerRect.top;

	var scaledMouseX = ((offsetMouseX / screenWidth) * 2.0) - 1.0;
	var scaledMouseY = (-(offsetMouseY / screenHeight) * 2.0) + 1.0;

	raycaster.setFromCamera(
		new three.Vector2(
			scaledMouseX,
			scaledMouseY
		),
		camera
	);

	// Calculate objects intersecting the picking ray
	var intersectList = raycaster.intersectObjects(scene.children, true);

	// intersectList is sorted with closest first 
	while(intersectList.length > 0){
		var intersection = intersectList[0];
		
		// Skip if collision not enabled
		if(!intersection.object.userData.collision){
			intersectList.shift();
			continue;
		}

// 		colorFace(intersection.face, 0xff0000);
// 		intersection.object.geometry.colorsNeedUpdate = true;

		// Do different things based on what was selected
		if(intersection.object.userData.gameObjectType === constants.TYPE_TILE){

			selectedTile = intersection.object;
			selectedFace = intersection.face;

			var tileSpider = spiderFactory.getSpiderAtTile(selectedTile);

			if(fireMode === false){
				// JUMP

				// Only jump to face with no spider
				// if(tileSpider){return;}

				// Only jump if at rest
				if(selectedSpider.userData.jumping){return;}

				// Only jump to upward face
				if(selectedFace.normal.y != 1.0){return;}

				// Only jump to tile within N units of distance
				console.log("Jump");
				var distanceX = Math.abs(selectedTile.userData.gridCoordinates.x - selectedSpider.userData.tile.userData.gridCoordinates.x);
				var distanceY = Math.abs(selectedTile.userData.gridCoordinates.y - selectedSpider.userData.tile.userData.gridCoordinates.y);
				var distanceZ = Math.abs(selectedTile.userData.gridCoordinates.z - selectedSpider.userData.tile.userData.gridCoordinates.z);
				var distance = distanceX + distanceY + distanceZ;
				if(distance > 3.0){return;}
				if(distanceX > 2.0){return;}
				if(distanceZ > 2.0){return;}

				// Send websocket message
				connection.send(JSON.stringify({
					payload: {
						actionId: constants.ACTION_ID_JUMP,
						spiderUuid: selectedSpider.userData.uuid,
						tileKey: selectedTile.userData.tileKey
					}
				}));

				return;

			}else if(fireMode === true){
				// FIRE

				// Only jump to upward face
				if(selectedFace.normal.y != 1.0){return;}

				// Only shoot projectile  to tile within N units of distance
				console.log("Fire");

				if(
					tileFactory.confirmTargetableTile(
						selectedSpider.userData.tile.userData.gridCoordinates,
						selectedTile
					)
				){
					connection.send(JSON.stringify({
						payload: {
							actionId: constants.ACTION_ID_FIRE,
							spiderUuid: selectedSpider.userData.uuid,
							tileKey: selectedTile.userData.tileKey
						}
					}));
				}

				return;
			}

		}
		else if(intersection.object.userData.gameObjectType === constants.TYPE_SPIDER){

			console.log("collided with spider");
			if(intersection.object === selectedSpider){
				console.log("jump");

				// Only jump if at rest
				if(selectedSpider.userData.jumping){return;}

				connection.send(JSON.stringify({
					payload: {
						actionId: constants.ACTION_ID_JUMP,
						spiderUuid: selectedSpider.userData.uuid,
						tileKey: selectedSpider.userData.tile.userData.tileKey
					}
				}));
			}

			return;
		}

		// Pop this intersection off if no action is to be taken
		intersectList.shift();

	}

}


function colorFace(face, color){

	for(v = 0; v < 3; v++){
		if(face.vertexColors[v]){face.vertexColors[v].setHex(color);}
		else{face.vertexColors[v] = new three.Color(color);}
	}

}


function animate(){

	// Clamp camera to player position
	if(selectedSpider && fireMode === false){
		cameraSwivelObjectBase.position.copy(selectedSpider.position);
		cameraSwivelObjectBase.position.y = 100.0;
	}

	// Check keyboard
	checkKeyboard();

	// Check keyboard
	checkGamepad();

	// Check touch
	if(mouseIsDown === true){
		mouseDownCounter++;
	}

	// Update clientTime
	clientTime = getTime();

	// Render game state 100 milliseconds in the past
	var renderTime = clientTime - 100;

	// Which snapshots is this time between?
	var snapshotA = null;
	var snapshotB = null;
	var a = 0;
	for(s in snapshotBuffer){
		var snapshot = snapshotBuffer[s];
		if(renderTime > snapshot.clientTime){
			snapshotA = snapshot;
			snapshotAIndex = s;
		}
	}

	// Perform actions if we've found a valid snapshot
	if(snapshotA){
		try{
			// See if valid snapshotB exists
			snapshotB = snapshotBuffer[a + 1];
			var snapshotTimeDifference = snapshotB.clientTime - snapshotA.clientTime;
			var offset = renderTime - snapshotA.clientTime;
			var scaledTime = offset / snapshotTimeDifference;

			var spiderBufferA = snapshotA.spiderBuffer;
			var spiderBufferB = snapshotB.spiderBuffer;

			// Delete local spiders that weren't referenced in the snapshot
			for(var uuid in spiderFactory.spiderDictionary){
				if(!spiderBufferA[uuid]){
					spiderFactory.spiderDictionary[uuid].userData.deactivate();
				}
			}

			// Interpolate spiders
			for(var uuid in spiderBufferA){

				if(spiderBufferA[uuid] && spiderBufferB[uuid]){

					// Create new spider if necessary
					var spider = spiderFactory.spiderDictionary[uuid];
					if(!spider){
						spider = spiderFactory.getFreeSpider();

						// If its the user's spider, select it at highlight its moveable spaces
						spider.userData.activate(uuid);
						if(spider.userData.uuid === connection.sessionId){
							selectSpider(spider);
							spider.userData.tile = tileFactory.getTileWithKey(
								spiderBufferA[uuid][6]
							);
							tileFactory.highlightMoveableTiles(
								spider.userData.tile.userData.gridCoordinates,
								0x0000ff
							);
						}
					}

					// Interpolate position
					var posA = new three.Vector3(
						spiderBufferA[uuid][0],
						spiderBufferA[uuid][1],
						spiderBufferA[uuid][2]
					);

					var posB = new three.Vector3(
						spiderBufferB[uuid][0],
						spiderBufferB[uuid][1],
						spiderBufferB[uuid][2]
					);

					var interpolatedPosition = posA.clone()
						.sub(posB)
						.multiplyScalar(-scaledTime)
						.add(posA);

					spider.position.copy(interpolatedPosition);

					// Rotation
					spider.rotation.set(
						spiderBufferA[uuid][3],
						spiderBufferA[uuid][4],
						spiderBufferA[uuid][5]
					);


					// Update spider tile
					spider.userData.tile = tileFactory.getTileWithKey(
						spiderBufferA[uuid][6]
					);

					// Highlight moveable tiles if user's spider landed a jump
					var jumping = spiderBufferA[uuid][7];
					if(spider.userData.jumping === true && jumping === false && uuid === connection.sessionId){
						tileFactory.highlightMoveableTiles(
							spider.userData.tile.userData.gridCoordinates,
							0x0000ff
						);
					}

					// Jump flag
					spider.userData.jumping = jumping;

				}
			}


			var projectileBufferA = snapshotA.projectileBuffer;
			var projectileBufferB = snapshotB.projectileBuffer;

			// Delete local spiders that weren't referenced in the snapshot
			for(var uuid in projectileFactory.projectileDictionary){
				if(!projectileBufferA[uuid]){
					projectileFactory.projectileDictionary[uuid].userData.deactivate();
				}
			}

			// Interpolate projectiles
			for(var uuid in projectileBufferA){

				if(projectileBufferA[uuid] && projectileBufferB[uuid]){

					// Create new projectile if necessary
					var projectile = projectileFactory.projectileDictionary[uuid];
					if(!projectile){
						projectile = projectileFactory.getFreeProjectile();
						projectile.userData.activate(uuid);
					}

					// Interpolate position
					var posA = new three.Vector3(
						projectileBufferA[uuid][0],
						projectileBufferA[uuid][1],
						projectileBufferA[uuid][2]
					);

					var posB = new three.Vector3(
						projectileBufferB[uuid][0],
						projectileBufferB[uuid][1],
						projectileBufferB[uuid][2]
					);

					var interpolatedPosition = posA.clone()
						.sub(posB)
						.multiplyScalar(-scaledTime)
						.add(posA);

					projectile.position.copy(interpolatedPosition);

					// Rotation
					projectile.rotation.set(
						projectileBufferA[uuid][3],
						projectileBufferA[uuid][4],
						projectileBufferA[uuid][5]
					);

					// Update projectile tile
					var newTile = tileFactory.getTileWithKey(
						projectileBufferA[uuid][6]
					);

				}
			}
			

		}catch(err){
			// Catch out of range error
			// Error handling for when there aren't two snapshots to interpolate between
			// Disply lag icon?
			console.log("Error");
			console.log(err);
		}
	}

	// When is it OK to dispose of old snapshots?
	if(snapshotBuffer.length >= 5){
		snapshotBuffer.shift();
	}

	// Play mesh animations
	var dt = clock.getDelta();
	spiderFactory.playMeshAnimations(dt);

	// note: three.js includes requestAnimationFrame shim 
	requestAnimationFrame(animate);

	renderer.render(scene, camera);

}


// ====================================================================================================
// Helper functions
// ====================================================================================================


function getTime(){

	var date = new Date();
	return date.getTime();

}


// ====================================================================================================
// EOF
// ====================================================================================================
