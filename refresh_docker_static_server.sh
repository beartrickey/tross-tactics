#! /bin/bash

browserify src/client.js -o static/bundle.js
docker stop static_server
docker rm static_server
docker build -t static_server -f static_server.dockerfile .
docker run --name static_server -d -p 8181:8181 static_server

