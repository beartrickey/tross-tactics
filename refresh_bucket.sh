#! /bin/bash

browserify src/client.js -o static/bundle.js && \
ansible-playbook ansible/s3_upload.yml -i ansible/inventory/localhost && \
open http://archimedesbucket.s3-website-us-west-1.amazonaws.com/
